/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package genex.compiler.generate
import genex.compiler.parser_param.{ ASTParam }
import genex.compiler.ast.{ Node,Expression,NodeType,NodeBLR }
import genex.utile.{ U32string }
import genex.global.{ VM_CMD,CmdBase,CmdJump,CmdJpGN }
import std.collection.{ ArrayList }

public func generateBlr(astp:ASTParam,nd:Node,step:Int64){
    let nNode:NodeBLR = (nd as NodeBLR).getOrThrow();

    if(step == -1){
        //加入一个合并开始指令
        astp.cmdObj.append(CmdBase(VM_CMD.CMD_STK_MERGE_S));
        //记录依赖数据
        nNode.blrStartLen = astp.cmdObj.len();
        nNode.blrCountID = astp.blrCount;astp.blrCount++;
    }else if(step == 0){
        //插入一个跳转指令
        astp.cmdObj.insert(nNode.blrStartLen,CmdJump(UInt64(astp.cmdObj.len() - nNode.blrStartLen + 1)));
        //加入一个生成指令
        astp.cmdObj.append(
            CmdJpGN(
                UInt64(nNode.blrCountID),
                1,1,UInt64(astp.cmdObj.len() - nNode.blrStartLen - 1)
            )
        );
        //加入一个合并指令
        astp.cmdObj.append(CmdBase(VM_CMD.CMD_STK_MERGE_E));
    }
}