/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package genex.compiler.ast
import genex.compiler.parser_param.{ ASTParam }
import std.collection.{ ArrayList }

public class NodeMLR <: Expression{
    public init(sfn:Option<SemanticsFn>,gfn:Option<GenerateFn>){
        super(NodeType.NODE_MLR,sfn,gfn);
    }
    public func execute(astp:ASTParam):Int64 {
        var rtv:Int64 = -1;
        //if(!this.gfn.isNone()){this.gfn.getOrThrow()(astp,this,-1);}
        for (i in 0..this.mExpressions.size) { 
            if(this.mExpressions[i].getType() == NodeType.NODE_SUB){
                rtv = this.mExpressions[i].execute(astp); 
                if(rtv <= 0){
                    return rtv;
                }
            }
        }
        return super.execute(astp);
    }
    public var reversal:Bool = false;
    public let mExpressions:ArrayList<Expression> = ArrayList<Expression>();
};