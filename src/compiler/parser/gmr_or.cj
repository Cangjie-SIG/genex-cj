/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package genex.compiler.parser
import genex.compiler.parser_param.{ ASTParam }
import genex.compiler.ast.{ Expression,NodeOr,tokentypeToPrecedence }
import genex.compiler.semantics.{ semanticsOr }
import genex.compiler.generate.{ generateOr }
import genex.compiler.lex.{ Token,TokenType }

public func createOr(psobj:ASTParam,lv:Expression):Option<Expression>{
    let gmrObj:Parser = (psobj.gmrObj as Parser).getOrThrow()
    let eTmp:NodeOr = NodeOr(semanticsOr,generateOr);
    eTmp.lv = lv;
    var tk:Token = Token();
    if(gmrObj.nextToken(tk)){eTmp.setPos(Int64(tk.pos));// |
        var tmp:?Expression = None;
        if(gmrObj.peekToken(tk)){
            tmp = gmrObj.parserExpression(prec:tokentypeToPrecedence(tk.tkType));
            if(tmp.isNone()){
                //出现空语句，意味着底层构建出现错误
            }
            eTmp.rv = tmp;
        }else{
            //错误 - 运算符缺少右值
            psobj.errorMgr.pushError((eTmp.getPos(),4));
        }
    }
    return eTmp;
}