/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package genex.run_time.execute
import genex.config.{ VMrun_timeRANGE }
import genex.run_time.param.{ run_timeParam }
import genex.global.{ CmdBase,VM_CMD }

public func execute(rtPrm:run_timeParam,ncmd:CmdBase){
    match (ncmd.cmdType){
        case VM_CMD.CMD_PUSH_STR => {=>
            rtPrm.i += cmdPushStr(rtPrm,ncmd);
        }
        case VM_CMD.CMD_PUSH_RANGE => {=>
            rtPrm.i += cmdPushRange(rtPrm,ncmd);
        }
        case VM_CMD.CMD_GENERATE => {=>
            rtPrm.i += cmdGenerate(rtPrm,ncmd);
        }
        case VM_CMD.CMD_GN_S => {=>
            rtPrm.i += cmdGnS(rtPrm,ncmd);
        }
        case VM_CMD.CMD_JUMP => {=>
            rtPrm.i += cmdJump(rtPrm,ncmd);
        }
        case VM_CMD.CMD_JP_R => {=>
            rtPrm.i += cmdJpR(rtPrm,ncmd);
        }
        case VM_CMD.CMD_JP_GN => {=>
            rtPrm.i += cmdJpGn(rtPrm,ncmd);
        }
        case VM_CMD.CMD_CALL => {=>
            rtPrm.i += cmdCall(rtPrm,ncmd);
        }
        case VM_CMD.CMD_STK_MERGE_S => {=>
            rtPrm.i += cmdStkMergeS(rtPrm,ncmd);
        }
        case VM_CMD.CMD_STK_MERGE_E => {=>
            rtPrm.i += cmdStkMergeE(rtPrm,ncmd);
        }
        case _ => { =>
            //未知的指令
            rtPrm.m_fatalError = true;
        }
    }()
}

public func exeRun(rtPrm:run_timeParam){
    let len:VMrun_timeRANGE = rtPrm.cmd.len();
    while(rtPrm.i < len && !rtPrm.m_fatalError){
        execute(rtPrm,rtPrm.cmd.get(rtPrm.i));
    }
}