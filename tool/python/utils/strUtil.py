
"""
    @brief 判断输入的是否为数值
    @param 字符串
    @return 成功返回 true
""" 
def is_integer(value):
    try:
        int(value)
        return True
    except ValueError:
        return False