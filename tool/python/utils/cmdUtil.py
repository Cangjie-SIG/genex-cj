

INPUT_TIP = " (true/false,t/f,y/n,1/0): "

"""
    @brief 判断用户输入的字符串是否为真
    @param input_str 输入的字符串
""" 
def is_true(input_str):
    true_values = {'true', 't', 'yes', 'y', '1'}
    return input_str.strip().lower() in true_values

"""
    @brief 获取用户输入
""" 
def input_val():
    return input("").strip()

"""
    @brief 并判断用户输入是否为真
""" 
def input_true():
    return is_true(input_val())

"""
    @brief 按照键值对获取参数
    @note 以"-"开头的视为 key, 紧随项视为 value
    以"--"开头视为开关
"""
def get_args(lst):
    out_list = {}
    key = ""
    for e in lst:
        if e.startswith("--"):
            out_list[e[2:]] = True
        elif e.startswith("-"):
            key = e[1:]
        elif key != "":
            out_list[key] = e
    return out_list
