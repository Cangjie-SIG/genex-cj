# 工具箱

(*注' 工具箱中的所有工具都应该在默认目录中执行，若移动文件所在位置，工具可能无法实现原有目标* )

## `python\proj_clear_build_test.py` [推荐]

> `proj_clear_build_test.py` 用于快速清理、编辑、测试，本工具与`bat\build[*].bat`用途相同。本工具启动时会非递归的先行清理`build`目录中的可执行文件以及动态链接库。然后判断主要代码是否已经编译。若未编译则编译，若已编译则询问是否重新编译。编译结束后，工具会寻找出可以测试的项目路径，并且列出清单。使用者选择需要测试的项目路径后，脚本自动编译测试。测试结束后工具会将项目下的可执行文件拷贝到`build`目录中，以便用户进行手动处理。<br/>

+ `proj_clear_build_test.py` 依赖 `python`、`cjpm` ，请确保系统环境变量中包含相关内容。<br/>
+ 关于本工具的启动参数，详见 `--help`，可通过`python proj_clear_build_test.py --help -lang zh-cn`输出中文帮助文档。

## `python\proj_test_all.py`

> `proj_test_all.py` 用于快速测试所有单元测试，并输出相关报告。本工具启动时会判断主要代码是否已经编译。若未编译则编译，若已编译则询问是否重新编译。编译结束后，工具会寻找出可以测试的项目路径，并且列出清单。然后工具会根据清单顺序逐个执行单元测试，并且收集单元测试的结果汇总成一份报告文件。待所有单元测试都执行完成后，工具会在`python\report`目录下输出一份以结束时间命名的文件。开发者可以通过此文件了解所有单元测试的结果及相关数据。值得注意的是：当某个单元测试出现错误时脚本并不会停止测试，而是会继续进行其它测试，直到所有测试都结束后才停止。<br/>

+ `proj_test_all.py` 依赖 `python`、`cjpm` ，请确保系统环境变量中包含相关内容。<br/>
+ 关于本工具的启动参数，请在脚本内查看。（与`proj_clear_build_test.py`相比缺少`-sel`和`--help`，其余一致）

## `python\proj_ergodic_dir.py`

> `proj_ergodic_dir.py` 用于快速生成`src`目录下的目录文件树图，以及自动添加后缀备注。本工具处理结束后会在工具目录中生成`md`文档，其中包含目录树图、目录及文件树图以及简单的统计信息。

+ `proj_ergodic_dir.py` 依赖 `python` ，请确保系统环境变量中包含相关内容。<br/>
+ 本工具无启动参数，直接执行即可。

## `bat\build[*].bat`

> `build[*].bat` 用于快速清理、编辑、测试，各个实现的含义如下：<br/>
> 1. `build.bat` 英文语言版本；
> 2. `build_utf8_zh_cn.bat` 中文简体UTF-8编码版本；
> 3. `build_gbk_zh_cn.bat` 中文简体GBK编码版本；

+ `build[*].bat` 依赖 `cjpm` ，请确保系统环境变量中包含相关内容。<br/>